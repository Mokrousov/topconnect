package common.http.accounts

import spray.json.DefaultJsonProtocol

case class GroupEntity(corpGroup: String, corpBalance: String, corpRemark: String)
case class GetGroupsRes(groups: Seq[GroupEntity])
case class CardEntity(tsimid: String, onum: String, inum: String, enum: String, enumlist: String, cellid: String, cardBalance: String, corpBalance: String, corpGroup: String, corpMaxlimit: String, corpMinlimit: String, aserviceid: String, corpEnabled: String, corpTransaction: String, corpTransfered: String, corpAssignationDate: String)
case class GetSimsInGroupRes(sims: Seq[CardEntity])
case class MoveSimsInGroup(fromGroup: Int, toGroup: Int, onums: Seq[String])
case class AddSimsToGroup(toGroup: Int, onums: Seq[String])

sealed trait RemoveResult
case class SuccessRemove(onum: String) extends RemoveResult()
case class FailureRemove(onum: String, reason: String) extends RemoveResult()
case class ResponseRemove(success: Seq[SuccessRemove], failure: Seq[FailureRemove])

trait AccountProtocols extends DefaultJsonProtocol {
  implicit val groupEntity2RequestFormat = jsonFormat3(GroupEntity.apply)
  implicit val getGroupsRes2RequestFormat = jsonFormat1(GetGroupsRes.apply)
  implicit val cardEntity2RequestFormat = jsonFormat16(CardEntity.apply)
  implicit val getSimsInGroup2RequestFormat = jsonFormat1(GetSimsInGroupRes.apply)
  implicit val moveSimsInGroup2RequestFormat = jsonFormat3(MoveSimsInGroup.apply)

  implicit val successRemove2RequestFormat = jsonFormat1(SuccessRemove.apply)
  implicit val failureRemove2RequestFormat = jsonFormat2(FailureRemove.apply)
  implicit val responseRemove2RequestFormat = jsonFormat2(ResponseRemove.apply)
}
