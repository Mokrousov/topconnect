package common.http.telecom

import akka.actor.ActorRef
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{as, complete, entity, get, path, _}
import akka.http.scaladsl.server.Route
import com.solution.telecom.proto.SwitchMerchantToProvider
import common.utils.StatusApi

/**
  * Created by alexander on 25.04.17.
  */
trait TelecomApi extends TelecomProtocols {
  val router: ActorRef

  val telecomRouts: Route = {
    (get & path("telecom" / "status")) {
      complete(StatusApi.asMap)
    } ~
      (path("telecom" / "switch-provider") & post) {
        entity(as[SwitchTelecomProvider]) {
          req =>
            router ! SwitchMerchantToProvider.Request.newBuilder()
              .setMerchantId(req.merchantId)
              .setProvider(req.provider)
            .build()

            complete(s"Switch merchant: '${req.merchantId}' to provider: '${req.provider}'")
        }
      }
  }
}