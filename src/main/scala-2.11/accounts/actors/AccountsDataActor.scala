package accounts.actors

import java.util.{Date, UUID}
import java.util.concurrent.TimeUnit

import accounts.models.{AccountModel, HistoryHazelcast}
import accounts.proto.{AuditTopUpEvt, TopUpCardBalanceCmd}
import accounts.utils.{MerchantGroups, Tools}
import accounts.ws.{WsClient, WsProvider}
import akka.NotUsed
import akka.actor.{ActorRef, Props}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{RunnableGraph, Sink, Source}
import com.datastax.driver.core.{PreparedStatement, Session}
import com.hazelcast.core.{HazelcastInstance, IMap}
import com.solution.telecom.proto.GetHistoryProto.History
import com.solution.telecom.proto._
import common.cassandra.CassandraTools
import common.utils.Config
import common.utils.Implicits.{materializer, systemExecutionContext}

import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.util.{Failure, Success}

class AccountsDataActor(hazelcast: HazelcastInstance, session: Session) extends CassandraTools {
  val defaultCurrency = "EUR"
  val defaultHistoryPart = 30

  val merchantsSeparator: Map[String, String] = Config.topConnectAccounts.flatMap(acc => acc._2.map(merchId => merchId -> acc._1.tcUserName)).toMap
  val sourceActors: Map[String, ActorRef] = Config.topConnectAccounts.map(acc => {
    val accountName: String = acc._1.tcUserName
    val orderActor = context.actorOf(Props(new OrderActorV(hazelcast, WsProvider.clients(accountName))), s"orderActor-$accountName")

    val sink: Sink[TopUpCardBalanceCmd, NotUsed] = Sink.actorRef[TopUpCardBalanceCmd](orderActor, "READY")
    val source: RunnableGraph[ActorRef] = Source.actorRef[TopUpCardBalanceCmd](500, OverflowStrategy.dropNew).to(sink)
    val sourceActor: ActorRef = source.run()

    acc._1.tcUserName -> sourceActor
  }).toMap

  val selectAccount: PreparedStatement = session.prepare("SELECT * FROM accounts WHERE account_id=?")
  val selectIccidToAccount: PreparedStatement = session.prepare("SELECT * FROM iccid_to_account WHERE iccid =?")

  val insertTopUp: PreparedStatement = session.prepare("INSERT INTO audit (account_id, msisdn, amount, currency, date) VALUES (?,?,?,?,?)")

  override def receive: Receive = {
    case x: TopUpCardBalanceProto.Request => topUpCard(x)
    case x: GetCardBalanceProto.Request => getCardBalance(x)
    case x: GetHistoryProto.Request => getHistory(x)
    case x: GetProductByAccountProto.Request => getProductByAccountId(x)
  }

  // Help getter for routing to the current account topconnect
  def getWsClientByMerchantID(merchantId: String): WsClient = WsProvider.clients(merchantsSeparator(merchantId))
  def getSourceActorByMerchantID(merchantId: String): ActorRef = sourceActors(merchantsSeparator(merchantId))

  // -----------================== * ==================-----------

  def getProductByAccountId(proto: GetProductByAccountProto.Request): Unit = {
    val response = GetProductByAccountProto.Response.newBuilder()
    val account = selectAccount(proto.getTelecomAccountId)

    val result = account map {
      case Some(s) =>
        response.setSuccess(GetProductByAccountProto.Success.newBuilder()
          .setIccid(s.iccid)
          .setMerchantId(s.merchantId)
          .setMsisdn(s.msisdn)
        ).build()
      case None =>
        response.setError(ResponseError.Error.newBuilder()
          .setErrorCode(0)
          .setMessage("Account is not exists")
        ).build()
    }

    reply(result)
  }

  /**
    * Save information about topup
    * @param evt AuditTopUpEvt
    */
  def auditTopUp(evt: AuditTopUpEvt): Unit = {
    insertTopUp(evt.accountId, evt.msisdn, evt.amount, evt.currency)
  }

  /**
    * Steps:
    * 1. Get onum by accountId
    * 2. Set balance
    *
    * @param proto SetCardBalanceProto.Request
    */
  def topUpCard(proto: TopUpCardBalanceProto.Request): Unit = {
    val account = selectAccount(proto.getTelecomAccountId)
    val requestOwner = sender()

    account onComplete {
      case Success(s) => s match {
        case Some(acc) =>

          val amount = BigDecimal(
            proto.getAmount.getWhole,
            proto.getAmount.getFractional
          ).toString


          getSourceActorByMerchantID(acc.merchantId) ! TopUpCardBalanceCmd(
            proto.getId,
            acc.msisdn,
            amount,
            defaultCurrency,
            MerchantGroups.groups(acc.merchantId).toString,
            proto.getTelecomAccountId,
            requestOwner
          )

        case None =>
          log.error("Account is not exists")
      }
      case Failure(e) =>
        log.error(e.getMessage)
    }

  }

  /**
    * Steps:
    * 1. Get onum by accountId
    * 2. Get balance
    *
    * @param proto GetCardBalanceProto.Request
    */
  def getCardBalance(proto: GetCardBalanceProto.Request): Unit = {
    val response = GetCardBalanceProto.Response.newBuilder()
    val account = selectAccount(proto.getTelecomAccountId)

    val result = account flatMap {
      case Some(s) =>
        getWsClientByMerchantID(s.merchantId).getBalance(s.msisdn) map {
          case Left(l) =>
            response.setError(ResponseError.Error.newBuilder()
              .setErrorCode(0)
              .setMessage(l.message)
            ).build()
          case Right(r) =>
            response.setSuccess(GetCardBalanceProto.Success.newBuilder()
              .setBalance(r.balance.toString)
            ).build()
        }
      case None => Future(response.setError(ResponseError.Error.newBuilder()
        .setErrorCode(0)
        .setMessage("Account is not exists")
      ).build())
    }

    reply(result)
  }

  /**
    * Get history
    *
    * History getting by period and then put to hazelcast cash
    * where it live ttl times. Proto owner get hash for first
    * part of history from cash. It looks like linkedlist in
    * Java. Last part has 'last' hash.
    *
    * @param proto GetHistoryProto.Request
    */
  def getHistory(proto: GetHistoryProto.Request): Unit = {
    val divideDate = false
    val response = GetHistoryProto.Response.newBuilder()

    // Get account by account ID
    val account = selectAccount(proto.getTelecomAccountId)

    // Get period sequence
    def periodDates(from: String, to: String): Seq[String] = {
      @tailrec
      def daysCollection(day: String, ac: Seq[String]): Seq[String] = {
        if (day == to) {
          ac :+ day
        } else {
          daysCollection(Tools.nextDay(day), ac :+ day)
        }
      }

      daysCollection(from, Seq())
    }

    // Get daily history
    def dailyHistory(onum: String, date: String, merchantId: String): Future[Seq[History]] = {
      val from = date
      val to = if (divideDate) Tools.nextDay(from) else proto.getDateTo

      val wsClient = getWsClientByMerchantID(merchantId)

      val callsFuture = wsClient.getCallHistory(onum, from, to)
      val smsesFuture = wsClient.getSmsHistory(onum, from, to)
      val internetFuture = wsClient.getInternetHistory(onum, from, to)
      val rechargeFuture = wsClient.getRechargeHistory(onum, from, to)

      val calls: Future[Seq[History]] = callsFuture map (_.map(call =>
        GetHistoryProto.History.newBuilder().setCall(GetHistoryProto.Call.newBuilder()
          .setBnum(if(call.cdir == "I") call.anum else call.bnum)
          .setDuration(call.duration)
          .setCost(call.ccost)
          .setDate(Tools.toUTC(call.calldate))
          .setCurrency(call.curr)
          .setCdir(call.cdir)
        ).build
      ))

      val internet: Future[Seq[History]] = internetFuture map {
        internet =>
          if (internet.nonEmpty) {
            val cost = internet.map(_.cost.toDouble).sum
            val traffic = internet.map(
              session => {
                val outb = if (session.outb.isEmpty) 0 else session.outb.toInt
                val inb = if (session.inb.isEmpty) 0 else session.inb.toInt
                outb + inb
              }
            ).sum

            Seq(GetHistoryProto.History.newBuilder().setInternate(GetHistoryProto.Internate.newBuilder()
              .setCost(cost.toString)
              .setTraffic(traffic.toString)
              .setDate(from + " 00:00:00")
              .setCurrency(internet.head.curr)
            ).build)
          } else
            Seq[History]()
      }

      val smses: Future[Seq[History]] = smsesFuture map (_.map(sms =>
        GetHistoryProto.History.newBuilder().setSms(GetHistoryProto.Sms.newBuilder()
          .setBnum(sms.bnum)
          .setCost(sms.ccost)
          .setDate(Tools.toUTC(sms.calldate))
          .setCurrency(sms.curr)
        ).build
      ))

      val recharges: Future[Seq[History]] = rechargeFuture map (_.filter(_.`type` == "card").map(recharge => {
        GetHistoryProto.History.newBuilder().setRecharge(GetHistoryProto.Recharge.newBuilder()
          .setAmount(Tools.parseAmount(recharge.amount)._1)
          .setDate(Tools.toUTC(recharge.added))
          .setBalance(Tools.parseAmount(recharge.balance)._1)
          .setCurrency(Tools.parseAmount(recharge.balance)._2)
        ).build

      }
      ))

      Future.sequence(Seq(calls, internet, smses, recharges)).map(_.foldLeft(Seq[History]())(_ ++ _))
    }

    // Put history to hazelcast
    def setHistoryToHazelcast(historySeq: Seq[History], historyPartSize: Int, ttl: Long): (String, Int, Seq[History]) = {
      val hazelcastHistoryMap: IMap[String, HistoryHazelcast] = hazelcast.getMap("history")
      val startHash = UUID.randomUUID().toString
      val pages = historySeq.size / historyPartSize

      @tailrec
      def put(hash: String, historyPart: Seq[History]): String = {
        val splitHistory = historyPart.splitAt(historyPartSize)
        val nextHash = if (splitHistory._2.nonEmpty) UUID.randomUUID().toString else "last"

        if (historyPart.isEmpty)
          startHash
        else {
          val page = (historySeq.size - historyPart.size) / historyPartSize + 1
          hazelcastHistoryMap.set(hash, HistoryHazelcast(nextHash, splitHistory._1, pages, page), ttl, TimeUnit.HOURS)
          put(nextHash, splitHistory._2)
        }
      }

      if (pages == 0) {
        ("last", pages, historySeq)
      } else {
        val firstPage = historySeq.splitAt(historyPartSize)
        (put(startHash, firstPage._2), pages, firstPage._1)
      }
    }

    val prepareReply = if (proto.getHash.isEmpty)
    // The firs request for history
      account flatMap {
        case Some(acc) =>
          val allHistory: Future[Seq[History]] = if (divideDate) Future.sequence(periodDates(proto.getDateFrom, proto.getDateTo)
            .map(dailyHistory(acc.msisdn, _, acc.merchantId))).map(_.foldLeft(Seq[History]())(_ ++ _)) else dailyHistory(acc.msisdn, proto.getDateFrom, acc.merchantId)

          allHistory.flatMap(history => {
            val hazelcastLink = Future(setHistoryToHazelcast(history, defaultHistoryPart, 1L))
            hazelcastLink.map { hazelcastHistory => {
              response.setSuccess(GetHistoryProto.Success.newBuilder()
                .setHash(hazelcastHistory._1)
                .setPages(hazelcastHistory._2)
                .addAllHistory(hazelcastHistory._3)
                .setPage(1)
              ).build()
            }
            }
          })
        case None =>
          Future(
            response.setError(ResponseError.Error.newBuilder()
              .setErrorCode(0)
              .setMessage("Account is not exists")
            ).build()
          )
      }
    else {
      // Get part of history
      Future {
        val hazelcastHistoryMap: IMap[String, HistoryHazelcast] = hazelcast.getMap("history")
        val history = hazelcastHistoryMap.get(proto.getHash)

        if (history == null)
          response.setError(ResponseError.Error.newBuilder()
            .setErrorCode(0)
            .setMessage("History by hash is empty")
          ).build()
        else
          response.setSuccess(GetHistoryProto.Success.newBuilder()
            .setHash(history.hash)
            .setPages(history.pages)
            .addAllHistory(history.history)
            .setPage(history.page)
          ).build()
      }
    }

    reply(prepareReply)
  }

  // -----------================== * ==================-----------
  // Prepare statements

  /**
    *
    * @param accountId account identifier
    * @return Future option
    */
  def selectAccount(accountId: String): Future[Option[AccountModel]] = {
    one(session.executeAsync(selectAccount.bind(
      accountId
    ))) map {
      case Some(x) => Some(
        AccountModel(
          x.getString("account_id"),
          x.getString("merchant_id"),
          x.getString("iccid"),
          x.getString("msisdn"),
          x.getTimestamp("date")
        )
      )
      case None => None
    }
  }

  /**
    * Select accountId by iccid
    *
    * @param iccid product iccid (imsi)
    * @return Option future of string
    */
  def selectIccidToAccount(iccid: String): Future[Option[String]] = {
    one(session.executeAsync(selectIccidToAccount.bind(
      iccid
    ))) map {
      case Some(x) => Option(x.getString("account_id"))
      case None => None
    }
  }

  /**
    * Insert info about topup
    */
  def insertTopUp(accountId: String, msisdn: String, amount: BigDecimal, currency: String): Unit = {
    val date = new Date
    one(session.executeAsync(insertTopUp.bind(accountId, msisdn, amount.bigDecimal, currency, date)))
  }
}
