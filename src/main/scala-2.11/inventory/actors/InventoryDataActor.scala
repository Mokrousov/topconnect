package inventory.actors

import java.nio.ByteBuffer
import java.util.{Date, UUID}

import com.datastax.driver.core.{Row, Session}
import com.solution.telecom.proto.{GetAccountByIccidProto, ProductCountProto, ReserveProductsProto, ResponseError => AccountResponseError}
import com.solution.telecom.topconnect.proto._
import common.cassandra.CassandraTools
import common.utils.Error
import common.utils.Implicits.systemExecutionContext
import inventory.proto._

import scala.collection.JavaConversions._
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class InventoryDataActor(session: Session) extends CassandraTools {

  val insertProduct = session.prepare("INSERT INTO products (merchant_id, iccid, structure, date) VALUES (?,?,?,?)")
  val insertAccount = session.prepare("INSERT INTO accounts (account_id, merchant_id, iccid, msisdn, date) VALUES (?,?,?,?,?)")
  val insertIccidToAccount = session.prepare("INSERT INTO iccid_to_account (account_id, iccid) VALUES (?,?)")

  val deleteProduct = session.prepare("DELETE FROM products WHERE merchant_id=? AND iccid=?")

  val selectProduct = session.prepare("SELECT * FROM products WHERE merchant_id=? AND iccid=?")
  val selectSeveralProducts = session.prepare("SELECT * FROM products WHERE merchant_id=? LIMIT ?")
  val selectCountProducts = session.prepare("SELECT COUNT(*) FROM products WHERE merchant_id=?")
  val selectAccount = session.prepare("SELECT * FROM accounts WHERE account_id=?")
  val selectIccidToAccount = session.prepare("SELECT * FROM iccid_to_account WHERE iccid =?")

  override def receive: Receive = {
    case x: ReserveProductsProto.Request => reserve(x)
    case x: AddBatchProto.Request => insertRetailInventory(x)
    case x: ProductCountProto.Request => productCount(x)
    case x: GetAccountByIccidProto.Request => accountIdByIccid(x)
    case DirectReserveCmd(merchantId, products) => directReserve(merchantId, products)
  }
  // -----------================== * ==================-----------

  /**
    * Get accountId by iccid
    *
    * @param proto GetAccountByIccidProto.Request
    */
  def accountIdByIccid(proto: GetAccountByIccidProto.Request): Unit = {
    val response = GetAccountByIccidProto.Response.newBuilder()
    val accountId = selectIccidToAccount(proto.getIccid)

    val result = accountId map {
      case Some(x) => response.setSuccess(GetAccountByIccidProto.Success.newBuilder().setTelecomAccountId(x)).build()
      case None => response.setError(AccountResponseError.Error.newBuilder()
        .setErrorCode(0)
        .setMessage("There is no account by this iccid")
      ).build()
    }
    reply(result)
  }

  /**
    * Get count of free products by merchantId
    *
    * @param proto ProductCountProto.Request
    */
  def productCount(proto: ProductCountProto.Request): Unit = {
    val response = ProductCountProto.Response.newBuilder()
    val count = selectCountProducts(proto.getMerchantId)

    val result = count.map(c =>
      if (c == -1)
        response.setError(AccountResponseError.Error.newBuilder()
          .setErrorCode(0)
          .setMessage("There is not product by merchantId: " + proto.getMerchantId)
        ).build()
      else
        response.setSuccess(ProductCountProto.Success.newBuilder()
          .setCount(c)
        ).build()
    )

    reply(result)
  }

  /**
    * Insert batch products
    *
    * @param proto AddBatchProto.Request
    */
  def insertRetailInventory(proto: AddBatchProto.Request): Unit = {
    val response = AddBatchProto.Response.newBuilder()
    val products = proto.getProductsList

    val listResults = products.map(product => insertProduct(
      proto.getMerchantId,
      product.getIccid,
      product
    ))

    val isSuccess = Future.sequence(listResults)
    val prepareResult = isSuccess.map(result =>
      if (result.contains(false))
        Left(Error(0, "Problem with inserting products"))
      else
        Right()
    )

    val result = prepareResult.map {
      case Right(_) => response.setSuccess(AddBatchProto.Success.newBuilder()).build()
      case Left(e) => response.setError(ResponseError.Error.newBuilder()
        .setErrorCode(e.errorCode)
        .setMessage(e.errorMessage)
      ).build()
    }

    reply(result)
  }

  def directReserve(merchantId: String, products: Seq[inventory.models.Product]): Unit ={
    // Step 1
    val accounts = {
      val tuple = products.map(product => {
        val acc = "tc_" + UUID.randomUUID().toString
        val ia = insertAccount(acc, merchantId, product.iccid, product.msisdn)
        (acc, ia, product.iccid)
      })

      val isSuccess = Future.sequence(tuple.map(_._2))

      isSuccess.map(result =>
        if (result.contains(false))
          Left(Error(0, "Problem with inserting to accounts"))
        else
          Right(tuple.map(t => (t._1, t._3)))
      )
    }
    // Step 2
    val links = accounts.flatMap {
      case Right(r) =>
        val listResults = r.map(tuple => insertIccidToAccount(tuple._1, tuple._2))
        val isSuccess = Future.sequence(listResults)

        isSuccess.map(result =>
          if (result.contains(false))
            Left(Error(0, "Problem with inserting to link accountId to iccid"))
          else
            Right(r)
        )
      case Left(e) => Future(Left(e))
    }
    // Step 3
    reply(links.map {
      case Right(r) => DirectReserveSuccess(r)
      case Left(e) => DirectReserveFailure
    })
  }

  /**
    * Reserve product(s)
    * Steps:
    * 1. Select product from retail inventory
    * 2. Insert to accounts
    * 3. Insert link table
    * 3. Delete from retail inventory
    *
    * @param proto ReserveProductsProto.Response
    */
  def reserve(proto: ReserveProductsProto.Request) {
    val response = ReserveProductsProto.Response.newBuilder()

    // Step 1
    val products = selectSeveralProducts(proto.getMerchantId, proto.getAmount).map(productsList =>
      if (productsList.length == proto.getAmount)
        Right(productsList)
      else
        Left(Error(0, "Not enough products. Count: " + productsList.length))
    )

    // Step 2
    val accounts = products.flatMap {
      case Right(r) =>
        val tuple = r.map(product => {
          val acc = "tc_" + UUID.randomUUID().toString
          val ia = insertAccount(acc, product.getMerchantId, product.getIccid, product.getMsisdn)
          (acc, ia, product.getIccid)
        })
        val isSuccess = Future.sequence(tuple.map(_._2))

        isSuccess.map(result =>
          if (result.contains(false))
            Left(Error(0, "Problem with inserting to accounts"))
          else
            Right(tuple.map(t => (t._1, t._3)))
        )
      case Left(e) => Future(Left(e))
    }

    // Step 3
    val links = accounts.flatMap {
      case Right(r) =>
        val listResults = r.map(tuple => insertIccidToAccount(tuple._1, tuple._2))
        val isSuccess = Future.sequence(listResults)

        isSuccess.map(result =>
          if (result.contains(false))
            Left(Error(0, "Problem with inserting to link accountId to iccid"))
          else
            Right(r)
        )
      case Left(e) => Future(Left(e))
    }

    // Step 4
    val deleting = links.flatMap {
      case Right(r) =>
        val listResults = r.map(tuple => deleteProduct(proto.getMerchantId, tuple._2))
        val isSuccess = Future.sequence(listResults)

        isSuccess.map(result =>
          if (result.contains(false))
            Left(Error(0, "Problem with deleting products from retail inventory"))
          else
            Right(r.map(_._1))
        )
      case Left(e) => Future(Left(e))
    }

    val result = deleting.map {
      case Right(r) => response.setSuccess(ReserveProductsProto.Success.newBuilder()
        .addAllAccounts(r)
      ).build()
      case Left(e) => response.setError(AccountResponseError.Error.newBuilder()
        .setErrorCode(e.errorCode)
        .setMessage(e.errorMessage)
      ).build()
    }

    val senderResponse = sender()
    Try{Await.result(result, 8 seconds)} match {
      case Success(s) =>
        log.info("\nReply: \n" + s.getClass.getCanonicalName + "\n" + s.toString)
        senderResponse ! s
      case Failure(f) =>
        log.error("\nReply error: \n" + f.getMessage)
        senderResponse ! response.setError(AccountResponseError.Error.newBuilder()
          .setErrorCode(0)
          .setMessage(f.getMessage)
        ).build()
    }
  }

  // -----------================== * ==================-----------
  // Prepare statements

  /**
    * Insert product to retail inventory
    *
    * @param merchantId merchant identifier
    * @param iccid      product iccid
    * @param product    protobuf model of product
    * @return Future[Boolean] 'true' if insert or 'false' if not insert
    */
  def insertProduct(merchantId: String, iccid: String, product: ProductProto.Product): Future[Boolean] = {
    val date = new Date
    oneApply(session.executeAsync(insertProduct.bind(
      merchantId,
      iccid,
      ByteBuffer.wrap(product.toByteArray),
      date
    )))
  }

  /**
    * Insert account for taken product
    *
    * @param accountId  account identifier
    * @param merchantId merchant identifier
    * @param iccid      product iccid
    * @param msisdn     product phone number
    * @return Future[Boolean] 'true' if insert or 'false' if not insert
    */
  def insertAccount(accountId: String, merchantId: String, iccid: String, msisdn: String): Future[Boolean] = {
    val date = new Date
    oneApply(session.executeAsync(insertAccount.bind(
      accountId,
      merchantId,
      iccid,
      msisdn,
      date
    )))
  }

  /**
    * Inset into link table iccid -> accountId
    *
    * @param accountId account identifier
    * @param iccid     product iccid
    * @return Future[Boolean]
    */
  def insertIccidToAccount(accountId: String, iccid: String): Future[Boolean] = {
    oneApply(session.executeAsync(insertIccidToAccount.bind(
      accountId,
      iccid
    )))
  }

  /**
    * Delete product from retail inventory.
    *
    * @param merchantId merchant identifier
    * @param iccid      product iccid
    * @return Future[Boolean] Future[Boolean] 'true' if deleted or 'false' if not deleted
    */
  def deleteProduct(merchantId: String, iccid: String): Future[Boolean] = {
    oneApply(session.executeAsync(deleteProduct.bind(
      merchantId,
      iccid
    )))
  }

  /**
    * Get protobuf model of product from cassandra blob
    *
    * @param row option Result Row of cassandra
    * @return option protobuf model of Product
    */
  def structureToProduct(row: Option[Row]): Option[ProductProto.Product] = {
    row match {
      case Some(x) => Option(ProductProto.Product.parseFrom(x.getBytes("structure").array()))
      case None => None
    }
  }

  /**
    * Select several products from retail inventory
    *
    * @param merchantId merchnat identifier
    * @param amount     amount products
    * @return Future list of products
    */
  def selectSeveralProducts(merchantId: String, amount: Integer): Future[List[ProductProto.Product]] = {
    all(session.executeAsync(selectSeveralProducts.bind(
      merchantId,
      amount
    ))) map (rows => rows.map(row => ProductProto.Product.parseFrom(row.getBytes("structure").array())))
  }

  /**
    * Select one product from inventory
    *
    * @param merchantId merchant identifier
    * @param iccid      product identifier (imsi)
    * @return Future option of ProductProto.Product
    */
  def selectProduct(merchantId: String, iccid: String): Future[Option[ProductProto.Product]] = {
    one(session.executeAsync(selectProduct.bind(
      merchantId,
      iccid
    ))) map structureToProduct
  }

  /**
    * Select count of free products in retail inventory
    *
    * @param merchantId merchant identifier
    * @return Future[Int] count of free products
    */
  def selectCountProducts(merchantId: String): Future[Int] = {
    one(session.executeAsync(selectCountProducts.bind(
      merchantId
    ))).map {
      case Some(x) => x.getLong("count").toInt
      case None => -1
    }
  }

  /**
    * Select accountId by iccid
    *
    * @param iccid product iccid (imsi)
    * @return Option future of string
    */
  def selectIccidToAccount(iccid: String): Future[Option[String]] = {
    one(session.executeAsync(selectIccidToAccount.bind(
      iccid
    ))) map {
      case Some(x) => Option(x.getString("account_id"))
      case None => None
    }
  }
}
