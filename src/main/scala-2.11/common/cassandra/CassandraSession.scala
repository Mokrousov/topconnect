package common.cassandra

import java.net.{InetAddress, InetSocketAddress}

import akka.event.LoggingAdapter
import com.datastax.driver.core._
import com.datastax.driver.core.policies.{ConstantReconnectionPolicy, DowngradingConsistencyRetryPolicy, RoundRobinPolicy}
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

object CassandraSession {

  /**
    * Init cassandra session
    *
    * @return cassandra session
    */
  def initSession(log: LoggingAdapter): Session = {
    val regularConfig = ConfigFactory.load()
    val nodes = regularConfig.getStringList("cassandra.topconnect_gateway.nodes").toList
    val username = Option(regularConfig.getString("cassandra.topconnect_gateway.username"))
    val password = Option(regularConfig.getString("cassandra.topconnect_gateway.password"))
    val coreConnectionsPerHost = if (regularConfig.hasPath("cassandra.topconnect_gateway.coreConnectionsPerHost")) regularConfig.getInt("cassandra.topconnect_gateway.coreConnectionsPerHost") else -1
    val maxConnectionsPerHost = if (regularConfig.hasPath("cassandra.topconnect_gateway.maxConnectionsPerHost")) regularConfig.getInt("cassandra.topconnect_gateway.maxConnectionsPerHost") else -1
    val keyspaceName = regularConfig.getString("cassandra.topconnect_gateway.keyspaceName")
    val port = regularConfig.getInt("cassandra.topconnect_gateway.port")
    val consistencyLevel = ConsistencyLevel.valueOf(Option(regularConfig.getString("cassandra.topconnect_gateway.consistencyLevel")).getOrElse("QUORUM"))

    log.info("Initial nodes set: " + nodes)

    val builder = Cluster.builder.addContactPoints(convertToInetAddress(nodes)).withPort(port)

    username match {
      case Some(usr) => password match {
        case Some(psw) => builder.withCredentials(usr, psw)
        case None => log.info("The password for cassandra does not set")
      }
      case None => log.info("The user name for cassandra does not set")
    }

    val poolingOptions = new PoolingOptions
    val cluster = builder.build
    val metadata = cluster.getMetadata

    if (coreConnectionsPerHost > -1) {
      log.info("Core connections per host: " + coreConnectionsPerHost)
      poolingOptions.setCoreConnectionsPerHost(HostDistance.LOCAL, coreConnectionsPerHost)
    }

    if (maxConnectionsPerHost > -1) {
      log.info("Max connections per host: " + maxConnectionsPerHost)
      poolingOptions.setMaxConnectionsPerHost(HostDistance.LOCAL, maxConnectionsPerHost)
    }

    log.info("Consistency level: " + consistencyLevel)
    builder.withPoolingOptions(poolingOptions)
    builder.withLoadBalancingPolicy(new RoundRobinPolicy())
      .withRetryPolicy(DowngradingConsistencyRetryPolicy.INSTANCE)
      .withQueryOptions(new QueryOptions().setConsistencyLevel(consistencyLevel))
      .withReconnectionPolicy(new ConstantReconnectionPolicy(100L))

    log.info("Connected to cluster: " + metadata.getClusterName)

    for (host <- metadata.getAllHosts) {
      log.info(String.format("Datatacenter: %s; Host: %s; Rack: %s", host.getDatacenter, host.getAddress, host.getRack))
    }

    log.info("Cassandra Cluster initialized successfully")
    cluster.connect(keyspaceName)
  }

  def convertToInetSocketAddresses(input: List[String]): List[InetSocketAddress] = {
    input.map(address => {
      val iport = address.split(":")
      new InetSocketAddress(iport(0), iport(1).toInt)
    })
  }

  def convertToInetAddress(input: List[String]): List[InetAddress] = {
    input.map(address => {
      InetAddress.getByName(address)
    })
  }
}
