package common.http.accounts

import accounts.utils.MerchantGroups
import accounts.ws.{WsClient, WsProvider}
import akka.actor.ActorRef
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import common.http.error2RequestFormat
import common.utils.{Config, Error, StatusApi}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait AccountApi extends AccountProtocols {
  val router: ActorRef

  val accountRouts: Route = {
    (path("accounts" / "get_groups") & get) {
      val groups: Iterable[Future[Seq[WsClient#Group]]] = WsProvider.clients.values.map(_.getGroups)
      val futureList = Future.sequence(groups)

      onComplete(futureList) {
        case Success(value) =>
          complete(GetGroupsRes(value.flatten.map(group => GroupEntity(group.corp_group, group.corp_balance, group.corp_remark)).toSeq).toString)
        case Failure(ex) =>
          complete(Error(1, ex.getMessage))
      }

    } ~
      (path("accounts" / "get_sims_in_group" / IntNumber) & get) {
        { groupId =>
          val groups: Map[Int, String] = MerchantGroups.groups.groupBy(_._2).map(s => s._1 -> s._2.head._1)
          val merchantId = groups(groupId)

          Config.topConnectAccounts.find(_._2.contains(merchantId)) match {
            case Some(f) =>
              val accountName = f._1.tcUserName
              onComplete(WsProvider.clients(accountName).getSimsInGroup(groupId)) {
                case Success(value) =>
                  complete(GetSimsInGroupRes(value.map(sim => CardEntity(sim.tsimid, sim.onum, sim.inum, sim.enum, sim.enumlist, sim.cellid,
                    sim.card_balance, sim.corp_balance, sim.corp_group, sim.corp_maxlimit, sim.corp_minlimit, sim.aserviceid, sim.corp_enabled,
                    sim.corp_transaction, sim.corp_transfered, sim.corp_assignation_date
                  ))).toString)
                case Failure(ex) =>
                  complete(Error(1, ex.getMessage))
              }
            case None => complete(Error(1, s"Can not find group: '$groupId'"))
          }
        }
      } ~
      (path("accounts" / "move_to_group") & post) {
        entity(as[MoveSimsInGroup]) {
          req => {
            val groups: Map[Int, String] = MerchantGroups.groups.groupBy(_._2).map(s => s._1 -> s._2.head._1)

            if(!groups.contains(req.fromGroup)){
              complete(Error(1, s"Can not find group: '${req.fromGroup}'"))
            } else if(!groups.contains(req.toGroup)){
              complete(Error(1, s"Can not find group: '${req.toGroup}'"))
            } else {
              val merchantIdFrom = groups(req.fromGroup)
              val merchantIdTo = groups(req.toGroup)

              Config.topConnectAccounts.find(_._2.contains(merchantIdFrom)) match {
                case Some(from) =>
                  Config.topConnectAccounts.find(_._2.contains(merchantIdTo)) match {
                    case Some(to) =>
                      // The topConnect account must be same
                      if (from._1.tcUserName != to._1.tcUserName) {
                        complete(Error(1, s"Group '${req.fromGroup}' and '${req.toGroup}' are in different topConnect accounts"))
                      }

                      val removeRes: Seq[Future[RemoveResult]] = req.onums.map(onum => {
                        // Delete from group
                        val deleteRes: Future[Boolean] = WsProvider.clients(from._1.tcUserName).removeSimFromGroup(
                          req.fromGroup.toString,
                          onum,
                          "0", "0", "0"
                        )

                        deleteRes.flatMap(isDeleted => {
                          if (isDeleted) {
                            // Try add in new group
                            val addRes: Future[Either[WsClient#Error, WsClient#Card]] = WsProvider.clients(to._1.tcUserName).setSimInGroup(
                              req.toGroup.toString,
                              onum
                            )

                            addRes.map {
                              case Left(l) => FailureRemove(onum, s"Can not add $onum from the group ${req.toGroup}. $l")
                              case Right(r) => SuccessRemove(r.onum)
                            }

                          } else {
                            Future {
                              FailureRemove(onum, s"Can not delete $onum from the group ${req.fromGroup}")
                            }
                          }
                        })
                      })

                      val resp: Future[ResponseRemove] = Future.sequence(removeRes).map(results => {
                        results.foldLeft(ResponseRemove(Seq(), Seq())) {
                          (b, a) => {
                            a match {
                              case SuccessRemove(onum) => ResponseRemove(b.success :+ SuccessRemove(onum), b.failure)
                              case FailureRemove(onum, reason) => ResponseRemove(b.success, b.failure :+ FailureRemove(onum, reason))
                            }
                          }
                        }
                      })

                      onComplete(resp) {
                        case Success(value) =>
                          complete(value toString)
                        case Failure(ex) =>
                          complete(Error(1, ex.getMessage))
                      }

                    case None => complete(Error(1, s"Can not find group: '${req.toGroup}'"))
                  }
                case None => complete(Error(1, s"Can not find group: '${req.fromGroup}'"))
              }
            }
            }
        }
      }
  }
}
