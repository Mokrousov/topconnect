package common

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import common.actors.RootActor
import common.utils.Implicits.system
import kamon.Kamon

object Main extends App {
  val root = system.actorOf(Props[RootActor], "root")
  system.actorOf(Props(classOf[Terminator], root), "terminator")

  //Kamon.start()

  class Terminator(ref: ActorRef) extends Actor with ActorLogging {
    context watch ref

    def receive = {
      case Terminated(_) =>
        log.info("{} has terminated, shutting down system", ref.path)
        //Kamon.shutdown()
        context.system.terminate()
    }
  }
}
