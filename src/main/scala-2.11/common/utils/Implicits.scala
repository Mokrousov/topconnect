package common.utils

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

object Implicits {
  implicit val system = ActorSystem("application")
  implicit val systemExecutionContext = system.dispatcher
  implicit val materializer = ActorMaterializer()
  implicit val cassandraExecutionContext = system.dispatchers.lookup("cassandra-thread-pool-dispatcher")
  implicit val wsExecutionContext = system.dispatchers.lookup("ws-thread-pool-dispatcher")
}