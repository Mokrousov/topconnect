package inventory.proto

case class DirectReserveCmd(merchantId: String, products: Seq[inventory.models.Product])

sealed trait DirectReserveResponse
case class DirectReserveSuccess(accounts: Seq[(String, String)]) extends DirectReserveResponse
case class DirectReserveFailure() extends DirectReserveResponse