package accounts.models

trait WsModels {
  case class Error(message: String)
  case class Account(name: String, active: String, expire: String, balance: String, currency: String, orderid: String)
  case class Call(tsimid: String, calldate: String, onum: String, anum: String,  bnum: String, rnum: String, cdir: String, duration: String, billsec: String, ccost: String, curr: String, mcost: String, conn_cost: String, free_sec: String, bleg: Boolean, adest: String, bdest: String, rdest: String, imsi: String, uniqueid: String)
  case class Sms(tsimid: String, calldate: String, onum: String, anum: String, bnum: String, rnum: String, smst: String, ccost: String, curr: String, imsi: String, part: String)
  case class Internet(tsimid: String, calldate: String, onum: String, opercode: String, country: String, operator: String, ipaddr: String, sessionid: String, inb: String, outb: String, rate: String, cost: String, curr: String, imsi: String)
  case class MoneyTransfer(added: String, `type`: String, orderid: String, uname: String, address: String, command: String, inum: String, onum: String, balance: String, amount:String)
  case class Status(tsimid: String, aserviceid: String, inum: String, onum: String, prepayed: String, blocked: Boolean, balance: String, curr: String)
  case class Balance(balance: String, amount: String)
  case class Group(corp_group: String, corp_balance: String, corp_remark:String)
  case class Card(tsimid: String, onum: String, inum: String, enum: String, enumlist: String, cellid: String, card_balance: String, corp_balance: String, corp_group: String, corp_maxlimit: String, corp_minlimit: String, aserviceid: String, corp_enabled: String, corp_transaction: String, corp_transfered: String, corp_assignation_date: String)
}
