package common.http

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import common.utils.Config._
import akka.stream.{ActorMaterializer, Materializer}

class DevRestApi(system: ActorSystem, routerActor: ActorRef) extends Routes {
  override val router: ActorRef = routerActor
  implicit val actorSystem: ActorSystem = system
  implicit val materializer: Materializer = ActorMaterializer()

  Http().bindAndHandle(handler = routes, interface = httpInterface, port = httpPort)
}

object DevRestApi {
  def apply(system: ActorSystem, routerActor: ActorRef): DevRestApi = new DevRestApi(system, routerActor)
}