name := "topconnect_gateway"

version := "1.0"

scalaVersion := "2.11.8"

lazy val root = (project in file("."))
  .enablePlugins(UniversalPlugin, JavaServerAppPackaging)

mainClass in(Compile, packageBin) := Some("common.Main")

mainClass in(Compile, run) := Some("common.Main")

resolvers += "snapshot" at "http://nexus.pm7.com/repository/maven_snapshot/"

resolvers += "release" at "http://nexus.pm7.com/repository/maven_release/"

libraryDependencies ++= {
  val kamonV = "0.6.3"
  val akkaV = "2.5.3"
  val akkaHttpV = "10.0.9"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-cluster-metrics" % akkaV,
    "com.typesafe.akka" %% "akka-cluster" % akkaV,
    "com.typesafe.akka" %% "akka-slf4j" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV,

    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,

    "com.typesafe.play" %% "play-ahc-ws-standalone" % "1.0.1",
    "com.typesafe.play" %% "play-ws-standalone-xml" % "1.0.1",
    "com.typesafe.akka" %% "akka-testkit" % akkaV,
    "com.typesafe.akka" %% "akka-persistence-cassandra" % "0.54",

    "com.rabbitmq" % "amqp-client" % "3.6.5",
    "com.datastax.cassandra" % "cassandra-driver-core" % "3.1.1",
    "com.google.protobuf" % "protobuf-java" % "3.1.0",
    "ch.qos.logback" % "logback-classic" % "1.1.3",
    "joda-time" % "joda-time" % "2.9.9",
    "com.hazelcast" % "hazelcast" % "3.7.5",
    "org.aspectj" % "aspectjweaver" % "1.8.10",
    "io.kamon" % "sigar-loader" % "1.6.6-rev002",
    "ca.aaronlevin" %% "scala-gitrev" % "0.1.1",

    "io.kamon" %% "kamon-core" % kamonV,
    "io.kamon" %% "kamon-akka" % kamonV,
    "io.kamon" %% "kamon-statsd" % kamonV,

    "com.solution" %% "proto-topconnect_gateway" % "1.2",
    "com.solution" %% "proto-telecom" % "1.4.2",

    "org.scalactic" %% "scalactic" % "3.0.1",
    "org.scalatest" % "scalatest_2.11" % "3.0.1" % "test",
    "org.cassandraunit" % "cassandra-unit" % "3.1.1.0"
  )
}

javaOptions in Test ++= Seq(
  s"-Djava.library.path=${baseDirectory.value / "native" }",
  "-Dconfig.resource=test.conf"
)

fork in Test := true

// Mapping
import NativePackagerHelper._

mappings in Universal ++= directory("native")

javaOptions in Universal ++= Seq(
  "-Djava.library.path=../native"
)

mappings in Universal ++= {
  val configDir = sourceDirectory.value/"main"/"resources"
  for {
    file <- (configDir ** AllPassFilter).get
    relative <- file.relativeTo(configDir.getParentFile)
    mapping = file -> {"conf/"+relative.getName }
  } yield mapping
}