package accounts.ws

import accounts.models.WsModels
import akka.stream.scaladsl.{Flow, Sink}
import akka.util.ByteString
import com.solution.telecom.proto.GetCallHistory
import common.utils.Config._
import common.utils.Implicits.materializer
import common.utils.Implicits.wsExecutionContext
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.ws.StandaloneWSRequest
import play.api.libs.ws.XMLBodyReadables._
import play.api.libs.ws.XMLBodyWritables._
import play.api.libs.ws.ahc.{AhcWSClientConfig, StandaloneAhcWSClient}

import scala.concurrent.Future

class WsClient(val tcUserName: String, val tcPassword: String, val accountServiceId: String) extends WsModels {
  val wsClient = StandaloneAhcWSClient(AhcWSClientConfig())
  val logger: Logger = LoggerFactory.getLogger(WsClient.this.getClass)

  val topConnectUrl = "https://xml2.travelsim.com/tsim_xml/service/xmlgate"
  val client: StandaloneWSRequest = wsClient.url(topConnectUrl)
    .withQueryStringParameters("plain" -> "1", "uname" -> tcUserName, "upass" -> tcPassword)

  logger.info(s"Service is working in $env environment. Account name: $tcUserName")

  def getAccount: Future[Account] = {
    client.addQueryStringParameters("command" -> "account")
      .get() map { wsResponse => {

      val xml = wsResponse.body[scala.xml.Elem]
      val name = xml \\ "name" text
      val active = xml \\ "active" text
      val expire = xml \\ "expire" text
      val balance = (xml \\ "balance").text
      val currency = xml \\ "currency" text
      val orderid = xml \\ "orderid" text

      Account(name, active, expire, balance, currency, orderid)
    }
    }
  }

  def getCallHistoryPages(onum: String, started: String, finished: String, pageSize: Int)(f: Seq[GetCallHistory.Call] => Unit) = {
    val pattern = ("<tsimid>(.*)</tsimid>[ \n]*<calldate>(.*)</calldate>[ \n]*<onum>(.*)</onum>[ \n]*<anum>(.*)</anum>[ \n]*" +
      "<bnum>(.*)</bnum>[ \n]*<rnum>(.*)</rnum>[ \n]*<cdir>(.*)</cdir>[ \n]*<duration>(.*)</duration>[ \n]*<billsec>(.*)</billsec>[ \n]*" +
      "<ccost>(.*)</ccost>[ \n]*<curr>(.*)</curr>[ \n]*<mcost>(.*)</mcost>[ \n]*<conn_cost>(.*)</conn_cost>[ \n]*<free_sec>(.*)</free_sec>" +
      "[ \n]*<bleg>(.*)</bleg>[ \n]*<adest>(.*)</adest>[ \n]*<bdest>(.*)</bdest>[ \n]*<rdest>(.*)</rdest>[ \n]*<uniqueid>(.*)</uniqueid>" +
      "[ \n]*<imsi>(.*)</imsi>").r

    client.addQueryStringParameters("command" -> "gccdr", "onum" -> onum, "started" -> started, "finished" -> finished)
      .withMethod("GET")
      .stream() flatMap { res =>

      res.bodyAsSource.grouped(pageSize).runWith(Sink.foreach[Seq[ByteString]] {
        bytesSeq =>
          val strSeq = bytesSeq.map(_.utf8String)
          val calls: Seq[Call] = strSeq flatMap {
            str =>
              (pattern findAllMatchIn str) map {
                groups =>
                  Call(
                    groups.group(1),
                    groups.group(2),
                    groups.group(3),
                    groups.group(4),
                    groups.group(5),
                    groups.group(6),
                    groups.group(7),
                    groups.group(8),
                    groups.group(9),
                    groups.group(10),
                    groups.group(11),
                    groups.group(12),
                    groups.group(13),
                    groups.group(14),
                    groups.group(15).toBoolean,
                    groups.group(16),
                    groups.group(17),
                    groups.group(18),
                    groups.group(19),
                    groups.group(20)
                  )
              }
          }
          val page: Seq[GetCallHistory.Call] = calls map {
            call =>
              GetCallHistory.Call.newBuilder()
                .setBnum(call.bnum)
                .setDuration(call.duration)
                .setCost(call.ccost)
                .setDate(call.calldate)
                .setCurrency(call.curr)
                .setCdir(call.cdir)
                .build()
          }
          f(page)
      })
    }
  }

  def getCallHistory(onum: String, started: String, finished: String) = {
    val pattern = ("<tsimid>(.*)</tsimid>[ \n]*<calldate>(.*)</calldate>[ \n]*<onum>(.*)</onum>[ \n]*<anum>(.*)</anum>[ \n]*" +
      "<bnum>(.*)</bnum>[ \n]*<rnum>(.*)</rnum>[ \n]*<cdir>(.*)</cdir>[ \n]*<duration>(.*)</duration>[ \n]*<billsec>(.*)</billsec>[ \n]*" +
      "<ccost>(.*)</ccost>[ \n]*<curr>(.*)</curr>[ \n]*<mcost>(.*)</mcost>[ \n]*<conn_cost>(.*)</conn_cost>[ \n]*<free_sec>(.*)</free_sec>" +
      "[ \n]*<bleg>(.*)</bleg>[ \n]*<adest>(.*)</adest>[ \n]*<bdest>(.*)</bdest>[ \n]*<rdest>(.*)</rdest>[ \n]*<uniqueid>(.*)</uniqueid>" +
      "[ \n]*<imsi>(.*)</imsi>").r

    client.addQueryStringParameters("command" -> "gccdr", "onum" -> onum, "started" -> started, "finished" -> finished)
      .withMethod("GET")
      .stream() flatMap { res =>

      res.bodyAsSource.runWith(Sink.fold[Seq[Call], ByteString](Seq()) { (total, bytes) =>
        val str = bytes.utf8String

        val find = (pattern findAllMatchIn str) map {
          groups =>
            Call(
              groups.group(1),
              groups.group(2),
              groups.group(3),
              groups.group(4),
              groups.group(5),
              groups.group(6),
              groups.group(7),
              groups.group(8),
              groups.group(9),
              groups.group(10),
              groups.group(11),
              groups.group(12),
              groups.group(13),
              groups.group(14),
              groups.group(15).toBoolean,
              groups.group(16),
              groups.group(17),
              groups.group(18),
              groups.group(19),
              groups.group(20)
            )
        }
        total ++ find.toSeq
      })
    }
  }

  def getSmsHistory(onum: String, started: String, finished: String) = {
    val pattern = ("<tsimid>(.*)</tsimid>[ \n]*<calldate>(.*)</calldate>[ \n]*<onum>(.*)</onum>[ \n]*<anum>(.*)</anum>[ \n]*" +
      "<bnum>(.*)</bnum>[ \n]*<rnum>(.*)</rnum>[ \n]*<smst>(.*)</smst>[ \n]*<ccost>(.*)</ccost>[ \n]*<curr>(.*)</curr>[ \n]*" +
      "<imsi>(.*)</imsi>[ \n]*<part>(.*)</part>").r

    client.addQueryStringParameters("command" -> "gccdr", "onum" -> onum, "started" -> started, "finished" -> finished)
      .withMethod("GET")
      .stream() flatMap { res =>

      res.bodyAsSource.runWith(Sink.fold[Seq[Sms], ByteString](Seq()) { (total, bytes) =>
        val str = bytes.utf8String

        val find = (pattern findAllMatchIn str) map {
          groups =>
            Sms(
              groups.group(1),
              groups.group(2),
              groups.group(3),
              groups.group(4),
              groups.group(5),
              groups.group(6),
              groups.group(7),
              groups.group(8),
              groups.group(9),
              groups.group(10),
              groups.group(11)
            )
        }
        total ++ find.toSeq
      })
    }
  }

  def getInternetHistory(onum: String, started: String, finished: String) = {
    val pattern = ("<tsimid>(.*)</tsimid>[ \n]*<calldate>(.*)</calldate>[ \n]*<onum>(.*)</onum>[ \n]*" +
      "<opercode>(.*)</opercode>[ \n]*<country>(.*)</country>[ \n]*<operator>(.*)</operator>[ \n]*" +
      "<ipaddr>(.*)</ipaddr>[ \n]*<sessionid>(.*)</sessionid>[ \n]*<inb>(.*)</inb>[ \n]*<outb>(.*)</outb>[ \n]*" +
      "<rate>(.*)</rate>[ \n]*<cost>(.*)</cost>[ \n]*<curr>(.*)</curr>[ \n]*<imsi>(.*)</imsi>").r

    client.addQueryStringParameters("command" -> "gprscdr", "onum" -> onum, "started" -> started, "finished" -> finished)
      .withMethod("GET")
      .stream() flatMap { res =>

      res.bodyAsSource.runWith(Sink.fold[Seq[Internet], ByteString](Seq()) { (total, bytes) =>
        val str = bytes.utf8String

        val find = (pattern findAllMatchIn str) map {
          groups =>
            Internet(
              groups.group(1),
              groups.group(2),
              groups.group(3),
              groups.group(4),
              groups.group(5),
              groups.group(6),
              groups.group(7),
              groups.group(8),
              groups.group(9),
              groups.group(10),
              groups.group(11),
              groups.group(12),
              groups.group(13),
              groups.group(14)
            )
        }
        total ++ find.toSeq
      })
    }
  }

  def getRechargeHistory(onum: String, started: String, finished: String) = {
    val patternCard = ("<added>(.*)</added>[ \n]*<type>(.*)</type>[ \n]*<orderid>(.*)</orderid>[ \n]*<uname>(.*)</uname>[ \n]*" +
      "<address>(.*)</address>[ \n]*<command>(.*)</command>[ \n]*<inum>(.*)</inum>[ \n]*<onum>(.*)</onum>[ \n]*<balance>(.*)" +
      "</balance>[ \n]*<amount>(.*)</amount>").r

    val patternClient = ("<added>(.*?)</added>[ \n]*<type>(.*?)</type>[ \n]*<orderid>(.*)</orderid>[ \n]*<uname>(.*)</uname>[ \n]*" +
      "<address>(.*)</address>[ \n]*<command>(.*)</command>[ \n]*<balance>(.*)</balance>[ \n]*<amount>(.*)</amount>").r


    client.addQueryStringParameters("command" -> "sbalance", "onum" -> onum, "started" -> started, "finished" -> finished)
      .withMethod("GET")
      .stream() flatMap { res =>

      res.bodyAsSource.runWith(Sink.fold[Seq[MoneyTransfer], ByteString](Seq()) { (total, bytes) =>
        val str = bytes.utf8String

        val findClient = (patternClient findAllMatchIn str) map {
          groups =>
            MoneyTransfer(
              groups.group(1),
              groups.group(2),
              groups.group(3),
              groups.group(4),
              groups.group(5),
              groups.group(6),
              "",
              "",
              groups.group(7),
              groups.group(8)
            )
        }
        val findCard = (patternCard findAllMatchIn str) map {
          groups =>
            MoneyTransfer(
              groups.group(1),
              groups.group(2),
              groups.group(3),
              groups.group(4),
              groups.group(5),
              groups.group(6),
              groups.group(7),
              groups.group(8),
              groups.group(9),
              groups.group(10)
            )
        }

        total ++ findClient.toSeq ++ findCard.toSeq
      })
    }
  }

  def getBalance(onum: String): Future[Either[Error, Status]] = {
    client.addQueryStringParameters("command" -> "gbalance", "onum" -> onum)
      .get() map { wsResponse => {

      if ((wsResponse.body[scala.xml.Elem] \ "type").text == "ERROR") {
        val message = (wsResponse.body[scala.xml.Elem] \ "text").text
        Left(Error(message))
      } else {
        val xml = wsResponse.body[scala.xml.Elem]
        val tsimid = xml \\ "tsimid" text
        val aserviceid = xml \\ "aserviceid" text
        val inum = xml \\ "inum" text
        val onum = xml \\ "onum" text
        val prepayed = xml \\ "prepayed" text
        val blocked = (xml \\ "blocked").text.toBoolean
        val balance = (xml \\ "balance").text
        val curr = xml \\ "curr" text

        Right(Status(tsimid, aserviceid, inum, onum, prepayed, blocked, balance, curr))
      }
    }
    }
  }

  def setBalance(onum: String, amount: String, curr: String, orderid: String): Future[Either[Error, Balance]] = {
    client.addQueryStringParameters("command" -> "sbalance", "onum" -> onum, "amount" -> amount, "curr" -> curr, "orderid" -> orderid)
      .get() map { wsResponse => {

      if ((wsResponse.body[scala.xml.Elem] \ "type").text == "ERROR") {
        val message = (wsResponse.body[scala.xml.Elem] \ "text").text
        Left(Error(message))
      } else {
        val xml = wsResponse.body[scala.xml.Elem] \\ "card"
        val balance = xml \\ "balance" text
        val amount = xml \\ "amount" text

        Right(Balance(balance, amount))
      }
    }
    }
  }

  def getGroups: Future[Seq[Group]] = {
    client.addQueryStringParameters("command" -> "corp", "aserviceid" -> accountServiceId)
      .get() map { wsResponse => {
      (wsResponse.body[scala.xml.Elem] \\ "group") map {
        xml =>
          val corp_group = xml \\ "corp_group" text
          val corp_balance = (xml \\ "corp_balance").text
          val corp_remark = xml \\ "corp_remark" text

          Group(corp_group, corp_balance, corp_remark)
      }
    }
    }
  }

  def getSimsInGroup(corp_group: Int): Future[Seq[Card]] = {
    client.addQueryStringParameters("command" -> "corp", "aserviceid" -> accountServiceId, "corp_group" -> corp_group.toString)
      .get() map { wsResponse => {

      (wsResponse.body[scala.xml.Elem] \\ "card") map {
        xml =>
          val tsimid = xml \ "tsimid" text
          val onum = xml \ "onum" text
          val inum = xml \ "inum" text
          val enum = xml \ "enum" text
          val enumlist = xml \ "enumlist" text
          val cellid = xml \ "cellid" text
          val card_balance = (xml \ "card_balance").text
          val corp_balance = (xml \ "corp_balance").text
          val corp_group = (xml \ "corp_group").text
          val corp_maxlimit = (xml \ "corp_maxlimit").text
          val corp_minlimit = (xml \ "corp_minlimit").text
          val aserviceid = xml \ "aserviceid" text
          val corp_enabled = xml \ "corp_enabled" text
          val corp_transaction = (xml \ "corp_transaction").text
          val corp_transfered = (xml \ "corp_transfered").text
          val corp_assignation_date = xml \ "corp_assignation_date" text

          Card(tsimid, onum, inum, enum, enumlist, cellid, card_balance, corp_balance, corp_group, corp_maxlimit, corp_minlimit, aserviceid, corp_enabled, corp_transaction, corp_transfered, corp_assignation_date)
      }
    }
    }
  }

  def getSimInGroup(corp_group: Int, onum: String): Future[Card] = {
    client.addQueryStringParameters("command" -> "corp", "aserviceid" -> accountServiceId, "corp_group" -> corp_group.toString, "onum" -> onum)
      .get() map { wsResponse => {
      val xml = wsResponse.body[scala.xml.Elem] \ "card"
      val tsimid = xml \ "tsimid" text
      val onum = xml \ "onum" text
      val inum = xml \ "inum" text
      val enum = xml \ "enum" text
      val enumlist = xml \ "enumlist" text
      val cellid = xml \ "cellid" text
      val card_balance = (xml \ "card_balance").text
      val corp_balance = (xml \ "corp_balance").text
      val corp_group = (xml \ "corp_group").text
      val corp_maxlimit = (xml \ "corp_maxlimit").text
      val corp_minlimit = (xml \ "corp_minlimit").text
      val aserviceid = xml \ "aserviceid" text
      val corp_enabled = xml \ "corp_enabled" text
      val corp_transaction = (xml \ "corp_transaction").text
      val corp_transfered = (xml \ "corp_transfered").text
      val corp_assignation_date = xml \ "corp_assignation_date" text

      Card(tsimid, onum, inum, enum, enumlist, cellid, card_balance, corp_balance, corp_group, corp_maxlimit, corp_minlimit, aserviceid, corp_enabled, corp_transaction, corp_transfered, corp_assignation_date)
    }
    }
  }

  def removeSimFromGroup(corp_group: String, onum: String, corp_minlimit: String, corp_transaction: String, corp_maxlimit: String): Future[Boolean] = {
    client.addQueryStringParameters("command" -> "corp", "corp_enabled" -> "delete", "aserviceid" -> accountServiceId, "corp_group" -> corp_group, "onum" -> onum, "corp_minlimit" -> corp_minlimit, "corp_transaction" -> corp_transaction, "corp_maxlimit" -> corp_maxlimit)
      .get() map { wsResponse => {
      if ((wsResponse.body[scala.xml.Elem] \ "text").text == "Deleted") true else false
    }
    }
  }

  def setGroupBalance(groupid: String, amount: String, curr: String, orderid: String): Future[Balance] = {
    client.addQueryStringParameters("command" -> "sbalance", "aserviceid" -> accountServiceId, "groupid" -> groupid, "amount" -> amount, "curr" -> curr, "orderid" -> orderid)
      .get() map { wsResponse => {

      val xml = wsResponse.body[scala.xml.Elem] \\ "corp"
      val balance = xml \\ "balance" text
      val amount = xml \\ "amount" text

      Balance(balance, amount)
    }
    }
  }

  def setGroup(corp_group: String, corp_remark: String): Future[Group] = {
    client.addQueryStringParameters("command" -> "corp", "aserviceid" -> accountServiceId, "corp_group" -> corp_group, "corp_remark" -> corp_remark)
      .get() map { wsResponse => {
      val xml = wsResponse.body[scala.xml.Elem] \ "group"
      val corp_group = xml \ "corp_group" text
      val corp_balance = (xml \ "corp_balance").text
      val corp_remark = xml \ "corp_remark" text

      Group(corp_group, corp_balance, corp_remark)
    }
    }
  }

  def setSimInGroupBalance(onum: String, amount: String, curr: String, groupid: String, orderid: String): Future[Either[Error, Balance]] = {
    client.addQueryStringParameters("command" -> "sbalance", "aserviceid" -> accountServiceId, "onum" -> onum, "amount" -> amount, "curr" -> curr, "groupid" -> groupid, "orderid" -> orderid)
      .get() map { wsResponse => {

      if ((wsResponse.body[scala.xml.Elem] \ "type").text == "ERROR") {
        val message = (wsResponse.body[scala.xml.Elem] \ "text").text
        Left(Error(message))
      } else {
        val xml = wsResponse.body[scala.xml.Elem] \\ "card"
        val balance = xml \\ "balance" text
        val amount = xml \\ "amount" text

        Right(Balance(balance, amount))
      }
    }
    }
  }

  def setSimInGroup(corp_group: String, onum: String): Future[Either[Error, Card]] = {
    client.addQueryStringParameters("command" -> "corp", "aserviceid" -> accountServiceId, "corp_minlimit" -> "0", "corp_transaction" -> "0", "corp_maxlimit" -> "0", "corp_enabled" -> "no", "corp_group" -> corp_group, "onum" -> onum)
      .get() map { wsResponse => {
      if ((wsResponse.body[scala.xml.Elem] \ "type").text == "ERROR") {
        val message = (wsResponse.body[scala.xml.Elem] \ "text").text
        Left(Error(message))
      } else {
        val xml = wsResponse.body[scala.xml.Elem] \ "card"
        val tsimid = xml \ "tsimid" text
        val onum = xml \ "onum" text
        val inum = xml \ "inum" text
        val enum = xml \ "enum" text
        val enumlist = xml \ "enumlist" text
        val cellid = xml \ "cellid" text
        val card_balance = (xml \ "card_balance").text
        val corp_balance = (xml \ "corp_balance").text
        val corp_group = (xml \ "corp_group").text
        val corp_maxlimit = (xml \ "corp_maxlimit").text
        val corp_minlimit = (xml \ "corp_minlimit").text
        val aserviceid = xml \ "aserviceid" text
        val corp_enabled = xml \ "corp_enabled" text
        val corp_transaction = (xml \ "corp_transaction").text
        val corp_transfered = (xml \ "corp_transfered").text
        val corp_assignation_date = xml \ "corp_assignation_date" text

        Right(Card(tsimid, onum, inum, enum, enumlist, cellid, card_balance, corp_balance, corp_group, corp_maxlimit, corp_minlimit, aserviceid, corp_enabled, corp_transaction, corp_transfered, corp_assignation_date))
      }
    }
    }
  }
}
