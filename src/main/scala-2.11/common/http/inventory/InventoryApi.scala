package common.http.inventory

import akka.actor.ActorRef
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import com.solution.telecom.topconnect.proto.{AddBatchProto, ProductProto}
import common.http.error2RequestFormat
import common.utils.Error
import inventory.proto.{DirectReserveCmd, DirectReserveFailure, DirectReserveSuccess}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

trait InventoryApi extends InventoryProtocols {

  val router: ActorRef
  implicit val timeout: Timeout = Timeout(5 seconds)

  val inventoryRouts: Route =
    (path("inventory" / "add-product") & post) {
      entity(as[AddProductsBatchRequest]) {
        req => {
          val request = AddBatchProto.Request.newBuilder()
            .setMerchantId(req.merchantId)

          for (product <- req.products) {
            request.addProducts(ProductProto.Product.newBuilder()
              .setMerchantId(req.merchantId)
              .setIccid(product.iccid)
              .setMsisdn(product.msisdn)
            )
          }

          onComplete(router ? request.build()) {
            case Success(value) =>
              value match {
                case response: AddBatchProto.Response => response.getResultCase.name match {
                  case "SUCCESS" => {
                    val success = response.getSuccess
                    complete("The products has been adding")
                  }
                  case "ERROR" => {
                    val error = response.getError
                    complete(Error(error.getErrorCode, error.getMessage))
                  }
                }
              }
            case Failure(ex) => complete(Error(1, ex.getMessage))
          }
        }
      }
    } ~
      (path("inventory" / "add-to-reserve") & post) {
        entity(as[AddProductsBatchRequest]) {
          req => {
            onComplete(router ? DirectReserveCmd(req.merchantId, req.products.map(prod => inventory.models.Product(prod.iccid, prod.msisdn)))) {
              case Success(s) => s match {
                case DirectReserveSuccess(res) => complete(res)
                case DirectReserveFailure() => complete("Error! The products has not been added")
              }
              case Failure(_) => complete("Error! The products has not been added")
            }
          }
        }
      }
}
