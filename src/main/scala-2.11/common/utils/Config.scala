package common.utils

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConverters._


object Config {

  case class TopConnectAccount(tcUserName: String, tcPassword: String, accountServiceId: String)

  protected val config = ConfigFactory.load()
  private val httpConfig = config.getConfig("http")
  private val topConnect = config.getConfig("topconnect")

  val httpInterface = httpConfig.getString("interface")
  val httpPort = httpConfig.getInt("port")

  // TopConnect configs
  // Step 1: Select env
  val env: String = topConnect.getString("env_conf.use")

  // Step 2: Set access for topconnect
  val topConnectAccounts = if (env == "prod") {
    val prodAccounts = topConnect.getConfigList("env_conf.prod").asScala.toList
    prodAccounts.map(conf => {
      (TopConnectAccount(conf.getString("uname"), conf.getString("upass"), conf.getString("account_service_id")), conf.getStringList("merchants"))
    })
  } else {
    val defAccounts = topConnect.getConfigList("env_conf.default").asScala.toList
    defAccounts.map(conf => {
      (TopConnectAccount(conf.getString("uname"), conf.getString("upass"), conf.getString("account_service_id")), conf.getStringList("merchants"))
    })
  }

  val hazelcastPort = config.getInt("hazelcast.topconnect_gateway.port")
  val merchants = config.getStringList("merchants").asScala.toList
  val serviceMainRole = config.getStringList("akka.cluster.roles").asScala.toList.head
}