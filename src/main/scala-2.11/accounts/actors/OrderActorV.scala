package accounts.actors

import accounts.proto._
import accounts.utils.Tools
import accounts.ws.WsClient
import akka.actor.{Actor, ActorLogging, ActorRef}
import com.hazelcast.core.{HazelcastInstance, IAtomicLong, ILock}
import com.solution.telecom.proto.{ResponseError, TopUpCardBalanceProto}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}
import common.utils.Implicits.systemExecutionContext

class OrderActorV(hazelcast: HazelcastInstance, wsClient: WsClient) extends Actor with ActorLogging {

  val lock: ILock = hazelcast.getLock(s"lock-${wsClient.tcUserName}")
  val orderId: IAtomicLong = hazelcast.getAtomicLong(s"orderId-${wsClient.tcUserName}")

  override def receive: Receive = {
    case cmd: TopUpCardBalanceCmd => processingTopUp(cmd)
  }

  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    initOrderId.onComplete {
      case Success(s) =>
        val id = s.toInt + 1
        orderId.set(id)
        log.info(s"Order actor has been init! $id")
      case Failure(e) =>
        log.error(s"Order actor has not been init! ${e.getMessage}")
    }
  }

  def initOrderId: Future[String] = {
    wsClient.getAccount.map(acc => acc.orderid)
  }

  /**
    * Find order id in error message from TopConnect.
    * If order id has not fined then return -1
    *
    * @param msg Error message for finding order id
    * @return TopConnect order id
    */
  def orderIdFromErrorMessage(msg: String): Long = {
    val pattern = "must be: ([0-9]+)".r
    pattern.findFirstMatchIn(msg) match {
      case Some(m) => m.group(1).toLong
      case None => -1
    }
  }

  /**
    * Try to resolve for order id problem
    *
    * @param errorMsg Message error from TopConnect (maybe contain order id)
    */
  def resolverOrderIdProblem(errorMsg: String, cmd: TopUpCardBalanceCmd, accountingSender: ActorRef): Unit = {
    val orderIdFromMessage = orderIdFromErrorMessage(errorMsg)
    if (orderIdFromMessage != -1) {
      // If we can take order id from response
      // Set hazelcast counter to new value
      orderId.set(orderIdFromMessage)
      log.info(s"The order id has been set in $orderIdFromMessage")
      // Send command again for processing
      self ! cmd
    } else {
      // If response does not contain order id
      // Prepare response with error
      log.info(s"The problem bears a different character (does not contain order id).")
      val senderResponse = TopUpCardBalanceProto.Response.newBuilder()
        .setId(cmd.requestId)
        .setError(ResponseError.Error.newBuilder()
          .setErrorCode(0)
          .setMessage(errorMsg)
        )
      // Send error to cmd owner
      cmd.cmdOwner ! senderResponse.build
    }
  }


  def processingTopUp(cmd: TopUpCardBalanceCmd): Unit = {
    // Accounting sender
    val accountingSender = sender()
    // Prepare response
    val senderResponse = TopUpCardBalanceProto.Response.newBuilder().setId(cmd.requestId)

    // Block atomic order ID
    lock.lock()

    val request = wsClient.setSimInGroupBalance(
      cmd.onum,
      Tools.rountingDown(cmd.amount),
      cmd.currency,
      cmd.group,
      orderId.get().toString
    )

    Try(Await.result(request, 8 seconds)) match {
      case Success(s) =>
        // Increment order ID
        orderId.incrementAndGet()
        s match {
          case Left(l) =>
            // Problem with setting balance. Try to resolve it
            log.error("Problem with setting balance. Try to resolve it ... Error from response: " + l.message)
            resolverOrderIdProblem(l.message, cmd, accountingSender)
          case Right(r) =>
            log.info(s"Balance has been set. Amount: ${r.amount}, balance: ${r.balance}")

            senderResponse.setSuccess(TopUpCardBalanceProto.Success.newBuilder()
              .setTelecomAccountId(cmd.accountId)
              .setBalance(r.balance)
            )
            cmd.cmdOwner ! senderResponse.build
            accountingSender ! AuditTopUpEvt(cmd.accountId, cmd.onum, BigDecimal(cmd.amount), cmd.currency)
        }

      case Failure(f) =>
        // Problem with setting balance. Try to resolve it
        log.error("Problem with setting balance _. Try to resolve it ... Error from response: " + f.getMessage)
        resolverOrderIdProblem(f.getMessage, cmd, accountingSender)
    }

    // Unblock atomic order ID
    lock.unlock()
  }
}
