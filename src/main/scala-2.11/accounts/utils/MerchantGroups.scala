package accounts.utils

object MerchantGroups {
  val groups = Map(
    "de6fee94-b2b5-4231-9158-2127ddcfd356" -> 10, // TestMerchant
    "95a530a8-c59c-4904-b7e2-c53acb81a9fc" -> 20, // PayPM Test
    "336662b1-2501-4592-ae81-77fd467b6e94" -> 21, // PayPm
    "080c9ddf-7899-4913-85c9-96e7c28d907c" -> 30, // Heimat Test
    "7cbd3eb8-e22f-4b45-8ae9-0fec226a4afb" -> 31, // Heimat Karpuz
    "f930aa19-0ecc-4a93-b028-cdf700be90df" -> 32, // Heimat Incir
    "85adbf98-c221-4d2c-9d14-5d3ef2547060" -> 33, // Heimat Ceviz
    "ec5ff63b-6ce8-4c7a-b581-f0228395081f" -> 34, // Heimat Ahududu
    "717d8a66-3f64-4437-8ead-716638a29c5f" -> 41, // Hallmark Lychee
    "49e20203-b57a-493a-bb92-391b06616576" -> 42, // Hallmark Koi
    "24857654-a571-457f-995b-11480b945f96" -> 43  // Mars

  )
}
