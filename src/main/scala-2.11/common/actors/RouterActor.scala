package common.actors

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.cluster.{Cluster, Member}
import com.google.protobuf.Message
import com.hazelcast.core.{HazelcastInstance, IMap}
import com.solution.telecom.proto._
import com.solution.telecom.topconnect.proto.AddBatchProto
import common.utils.Config
import inventory.proto.DirectReserveCmd

class RouterActor(inventoryDataActor: ActorRef, accountsDataActor: ActorRef, hazelcast: HazelcastInstance) extends Actor with ActorLogging {

  val merchantProviders: IMap[String, String] = hazelcast.getMap("merchant-provider")

  override def receive: Receive = {
    case cmd: SwitchMerchantToProvider.Request =>
      log.info(s"Switch merchant: '${cmd.getMerchantId}' to provider: '${cmd.getProvider}'")
      merchantProviders.put(cmd.getMerchantId, cmd.getProvider)
    case cmd: Message => receiveCMD(cmd)
    case cmd: DirectReserveCmd => forwardToInventoryDataActor(cmd)

    case x => log.info("Receive unknown protocol! " + x.getClass.getCanonicalName + "\nSender: \n" + sender().path)
  }

  def forwardToInventoryDataActor(request: Any): Unit = {
    log.info("\nSender: \n" + sender().path + " Receive: \n" + request.getClass.getCanonicalName + "\n" + request.toString)
    inventoryDataActor forward request
  }

  def forwardToAccountDataActor(request: Message): Unit = {
    log.info("\nSender: \n" + sender().path + " Receive: \n" + request.getClass.getCanonicalName + "\n" + request.toString)
    accountsDataActor forward request
  }

  def receiveCMD(cmd: Message): Unit = {
    val descriptorAccountIdOpt = Option(cmd.getDescriptorForType.findFieldByName("telecomAccountId"))
    val descriptorMerchantIdOpt = Option(cmd.getDescriptorForType.findFieldByName("merchantId"))

    val isCmdValid = descriptorAccountIdOpt match {
      case Some(_) => true
      case None => descriptorMerchantIdOpt match {
        case Some(_) => true
        case None => false
      }
    }

    if(isCmdValid){
      descriptorMerchantIdOpt match {
        case Some(descriptorMerchantId) =>
          val merchantId = cmd.getField(descriptorMerchantId).toString
          Option(merchantProviders.get(merchantId)) match {
            case Some(provider) =>
              if (provider == Config.serviceMainRole)
                processCommand(cmd)
              else {
                sendToDestination(provider, cmd)
              }
            case None => sendError(0, "Missing destination for merchantId")
          }
        case None => // Do nothing
      }

      descriptorAccountIdOpt match {
        case Some(descriptorAccountId) =>
          val accountId = cmd.getField(descriptorAccountId).toString
          accountId.split("_")(0) match {
            case "tc" => processCommand(cmd)
            case "c9" => sendToDestination("cloud9_gateway", cmd)
            case _ => sendError(0, "Missing destination for accountId or wrong accountId format")
          }
        case None => // Do nothing
      }
    } else {
      sendError(0, "Cmd not valid")
    }
  }

  def processCommand(cmd: Message): Unit ={
    cmd match {
      case x: AddBatchProto.Request => forwardToInventoryDataActor(x)
      case x: ReserveProductsProto.Request => forwardToInventoryDataActor(x)
      case x: ProductCountProto.Request => forwardToInventoryDataActor(x)
      case x: GetHistoryProto.Request => forwardToAccountDataActor(x)
      case x: GetAccountByIccidProto.Request => forwardToInventoryDataActor(x)

      case x: TopUpCardBalanceProto.Request => forwardToAccountDataActor(x)
      case x: GetProductByAccountProto.Request => forwardToAccountDataActor(x)
      case x: GetCardBalanceProto.Request => forwardToAccountDataActor(x)
      case x => log.info("Receive unknown protocol! " + x.getClass.getCanonicalName + "\nSender: \n" + sender().path)
    }
  }

  def sendToDestination(role: String, cmd: Message): Unit ={
    val cluster = Cluster(context.system)
    val providers: Set[Member] = cluster.state.members.filter(m => m.roles.contains(role))

    Option(providers.head.address) match {
      case Some(address) =>
        val selection = context.actorSelection(s"$address/user/root/routerActor")
        log.info(s"Redirect cmd: ${cmd.getClass.getCanonicalName} to $address/user/root/routerActor")
        selection forward cmd
      case None =>
        sendError(0, "Destination does not found")
    }
  }

  def sendError(code: Int, msg: String): Unit ={
    sender forward ResponseError.Error.newBuilder()
      .setErrorCode(code)
      .setMessage(msg)
      .build()
  }
}
