package common.http

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

trait InternalRouts {
  val internal: Route = {
    (path("internal" / "self-check") & get) {
      complete("OK")
    }
  }
}