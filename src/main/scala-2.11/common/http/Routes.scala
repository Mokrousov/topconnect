package common.http

import akka.http.scaladsl.server.Directives._
import common.http.accounts.AccountApi
import common.http.inventory.InventoryApi
import common.http.telecom.TelecomApi

trait Routes extends InventoryApi with AccountApi with TelecomApi with InternalRouts{
  val routes = pathPrefix("v1") {
    inventoryRouts ~ accountRouts ~ telecomRouts
  } ~ internal
}