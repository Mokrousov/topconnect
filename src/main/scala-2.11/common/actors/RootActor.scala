package common.actors

import accounts.actors.AccountsDataActor
import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import com.hazelcast.config.{Config => Hconfig}
import com.hazelcast.core.{Hazelcast, HazelcastInstance, IMap}
import common.cassandra.CassandraSession
import common.http.DevRestApi
import common.utils.Config
import inventory.actors.InventoryDataActor

case class TopActors(router: ActorRef, inventoryData: ActorRef)

class RootActor extends Actor with ActorLogging{
  def receive: Receive = {
    case t: Terminated => terminated(t)
  }

  def terminated(msg: Terminated) {
    context unwatch (msg actor)
  }

  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    val (topActors, hazelcast) = initTopActors
    DevRestApi(context.system, topActors.router)
    // Set merchant to hazelcast for subsequent telecom proto
    val merchantProviders: IMap[String, String] = hazelcast.getMap("merchant-provider")
    Config.merchants.foreach(merchant => merchantProviders.put(merchant, Config.serviceMainRole))
  }

  def initTopActors: (TopActors, HazelcastInstance) = {
    val hazelcastConf = new Hconfig()
    hazelcastConf.getGroupConfig.setName( "telecom" ).setPassword( "telecom-pass" )
    hazelcastConf.getNetworkConfig.setPort(Config.hazelcastPort).setPortAutoIncrement(false)
    hazelcastConf.getNetworkConfig.getJoin.getMulticastConfig.setEnabled(true)
    hazelcastConf.getNetworkConfig.getJoin.getTcpIpConfig.setEnabled(false)
    val hazelcast = Hazelcast.newHazelcastInstance(hazelcastConf)

    val cassandraSession = CassandraSession.initSession(log)

    val inventoryDataActor = context.actorOf(Props(new InventoryDataActor(cassandraSession)), "inventoryDataActor")
    val accountsDataActor = context.actorOf(Props(new AccountsDataActor(hazelcast, cassandraSession)), "accountsDataActor")
    val router = context.actorOf(Props(new RouterActor(inventoryDataActor, accountsDataActor, hazelcast)), "routerActor")

    (TopActors(router, inventoryDataActor), hazelcast)
  }
}
