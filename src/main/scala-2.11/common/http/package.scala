package common

import common.utils.Error
import spray.json.DefaultJsonProtocol

package object http extends DefaultJsonProtocol {
  implicit val error2RequestFormat = jsonFormat2(Error.apply)
}
