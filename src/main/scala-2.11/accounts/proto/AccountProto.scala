package accounts.proto

import akka.actor.ActorRef

case class TopUpCardBalanceCmd(requestId: String, onum: String, amount: String, currency: String, group: String, accountId: String, cmdOwner: ActorRef)
case class AuditTopUpEvt(accountId: String, msisdn: String, amount: BigDecimal, currency: String)