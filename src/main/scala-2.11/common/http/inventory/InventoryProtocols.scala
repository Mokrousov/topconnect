package common.http.inventory

import spray.json.DefaultJsonProtocol

case class Product(iccid: String, msisdn: String)
case class AddProductsBatchRequest(merchantId: String, products: Seq[Product])

trait InventoryProtocols extends DefaultJsonProtocol{
  implicit val product2RequestFormat = jsonFormat2(Product.apply)
  implicit val addProductsBatchRequest2RequestFormat = jsonFormat2(AddProductsBatchRequest.apply)
}
