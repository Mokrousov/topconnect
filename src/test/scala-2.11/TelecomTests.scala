import accounts.actors.{AccountsDataActor, OrderActorV}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.datastax.driver.core.Session
import com.hazelcast.config.{Config => Hconfig}
import com.hazelcast.core.{Hazelcast, HazelcastInstance}
import com.solution.telecom.proto._
import com.solution.telecom.topconnect.proto.AddBatchProto
import com.solution.telecom.topconnect.proto.ProductProto.Product
import common.actors.RouterActor
import common.utils.Config
import inventory.actors.InventoryDataActor
import org.cassandraunit.CQLDataLoader
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet
import org.cassandraunit.utils.EmbeddedCassandraServerHelper
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Future

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class TelecomTests extends TestKit(ActorSystem("application")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with Config{

  var embeddedSession: Session = _

  var inventoryDataActor: ActorRef = _
  var accountsDataActor: ActorRef = _
  var router: ActorRef = _
  var hazelcast: HazelcastInstance = _

  val goodIccid = "good_iccid"
  val goodMerchantId = "7cbd3eb8-e22f-4b45-8ae9-0fec226a4afb"
  val goodMsisdn = "37282788926"

  var goodTelecomAccountId: String = _
  var badTelecomAccountId: String = _
  var balance: BigDecimal = _

  override def beforeAll {
    // Start and init cassandra
    EmbeddedCassandraServerHelper.startEmbeddedCassandra()
    EmbeddedCassandraServerHelper.cleanEmbeddedCassandra()
    embeddedSession = EmbeddedCassandraServerHelper.getSession
    val dataLoader: CQLDataLoader = new CQLDataLoader(embeddedSession)
    dataLoader.load(new ClassPathCQLDataSet("cassandra_tables_for_tests.cql", true, "topconnect_gateway"))

    // Set account id in db
    embeddedSession.executeAsync("INSERT INTO topconnect_gateway.accounts (account_id, merchant_id, iccid, msisdn, date) VALUES ('tc_94d4e191-a0c4-4d0a-a9d0-3690c3dc70a3', '7cbd3eb8-e22f-4b45-8ae9-0fec226a4afb', '', '37282788926', dateof(now()));")
    embeddedSession.executeAsync("INSERT INTO topconnect_gateway.accounts (account_id, merchant_id, iccid, msisdn, date) VALUES ('tc_94d4e191-a0c4-4d0a-a9d0-3690c3dc70a4', '7cbd3eb8-e22f-4b45-8ae9-0fec226a4afb', '', '37111111111', dateof(now()));")

    // Start hazelcast
    val hazelcastConf = new Hconfig()
    hazelcastConf.getGroupConfig.setName( "topconnect_gateway").setPassword("topconnect_gateway-pass")
    hazelcastConf.getNetworkConfig.setPort(hazelcastPort)
    hazelcast = Hazelcast.newHazelcastInstance(hazelcastConf)

    // Start actors
    val orderActor = system.actorOf(Props(new OrderActorV(hazelcast)), "orderActor")
    inventoryDataActor = system.actorOf(Props(new InventoryDataActor(embeddedSession)), "inventoryDataActor")
    accountsDataActor = system.actorOf(Props(new AccountsDataActor(orderActor, hazelcast, embeddedSession)), "accountsDataActor")
    router = system.actorOf(Props(new RouterActor(inventoryDataActor, accountsDataActor)), "routerActor")

    // Wait when TopConnect return response to orderActor
    Thread.sleep(10000)
  }

  override def afterAll {
    TestKit.shutdownActorSystem(system)
    embeddedSession.close()
    hazelcast.shutdown()
  }

  "Prepare" must {
    "get balance" in {
      val req = GetCardBalanceProto.Request.newBuilder()
        .setTelecomAccountId("tc_94d4e191-a0c4-4d0a-a9d0-3690c3dc70a3")
        .build()

      router ! req

      val res = expectMsgType[GetCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          balance = BigDecimal(res.getSuccess.getBalance)
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "return money" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        "tc_94d4e191-a0c4-4d0a-a9d0-3690c3dc70a3",
        "EUR",
        balance.bigDecimal
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }
  }

  "Inventory" must {
    "add batch products" in {
      val req = AddBatchProto.Request.newBuilder()
        .addProducts(Product.newBuilder()
            .setIccid(goodIccid)
            .setMerchantId(goodMerchantId)
            .setMsisdn(goodMsisdn)
        ).addProducts(Product.newBuilder()
        .setIccid("bad_iccid")
        .setMerchantId(goodMerchantId)
        .setMsisdn("380500000000")
      ).setMerchantId(goodMerchantId)
        .build()

      router ! req

      val res = expectMsgType[AddBatchProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" =>
          assert(true)
        case "ERROR" =>
          assert(false)
      }
    }

    "get count of products" in {
      val req = ProductCountProto.Request.newBuilder()
        .setMerchantId(goodMerchantId)
        .build()

      router ! req

      val res = expectMsgType[ProductCountProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          val count = res.getSuccess.getCount
          assert(count == 2)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "reserve products" in {
      val req = ReserveProductsProto.Request.newBuilder()
        .setAmount(2)
        .setMerchantId(goodMerchantId)
        .build()

      router ! req

      val res = expectMsgType[ReserveProductsProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          badTelecomAccountId = res.getSuccess.getAccounts(0)
          goodTelecomAccountId = res.getSuccess.getAccounts(1)
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "get telecom account id by iccid" in {
      val req = GetAccountByIccidProto.Request.newBuilder()
        .setMerchantId(goodMerchantId)
        .setIccid(goodIccid)
        .build()

      router ! req

      val res = expectMsgType[GetAccountByIccidProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          val resTelecomAccountId = res.getSuccess.getTelecomAccountId
          assert(resTelecomAccountId == goodTelecomAccountId)
        }
        case "ERROR" =>
          assert(false)
      }
    }
  }

  "Telecom" must {
    "not topup wrong card balance" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        badTelecomAccountId,
        "EUR",
        BigDecimal("0.1").bigDecimal
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(false)
        }
        case "ERROR" =>
          assert(true)
      }
    }

    "topup card balance" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        goodTelecomAccountId,
        "EUR",
        BigDecimal("0.2").bigDecimal
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "get money from card balance" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        goodTelecomAccountId,
        "EUR",
        BigDecimal("-0.2").bigDecimal
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "get card balance" in {
      val req = GetCardBalanceProto.Request.newBuilder()
        .setTelecomAccountId(goodTelecomAccountId)
        .build()

      router ! req

      val res = expectMsgType[GetCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "get history" in {
      val req = GetHistoryProto.Request.newBuilder()
        .setTelecomAccountId(goodTelecomAccountId)
        .build()

      router ! req

      val res = expectMsgType[GetHistoryProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "get product by account" in {
      val req = GetProductByAccountProto.Request.newBuilder()
          .setTelecomAccountId(goodTelecomAccountId)
        .build()

      router ! req

      val res = expectMsgType[GetProductByAccountProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }
  }

  "TopUp" must {
    "do 500 positive scenarios & 500 negative scenarios" in {
      val reqGood = RichTopUpCardBalanceProto(
        "id",
        goodTelecomAccountId,
        "EUR",
        BigDecimal("0.01").bigDecimal
      ).build

      val reqBad = RichTopUpCardBalanceProto(
        "id",
        badTelecomAccountId,
        "EUR",
        BigDecimal("0.01").bigDecimal
      ).build

      Future {
        (0 until 500) foreach {
          _ => router ! reqGood
        }
      }

      Future {
        (0 until 500) foreach {
          _ => router ! reqBad
        }
      }

      val results = receiveN(1000,1000 seconds)
    }

    "get card balance" in {
      val req = GetCardBalanceProto.Request.newBuilder()
        .setTelecomAccountId(goodTelecomAccountId)
        .build()

      router ! req

      val res = expectMsgType[GetCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          balance = BigDecimal(res.getSuccess.getBalance)
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }

    "check balances before and after topups" in {
      assert(balance == BigDecimal(5))
    }

    "return money" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        goodTelecomAccountId,
        "EUR",
        balance.bigDecimal.negate()
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }
  }

  /*"Sets" must {
    "set balance to 0" in {
      val req = RichTopUpCardBalanceProto(
        "id",
        "tc_94d4e191-a0c4-4d0a-a9d0-3690c3dc70a3",
        "EUR",
        BigDecimal(-2.72).bigDecimal
      ).build

      router ! req

      val res = expectMsgType[TopUpCardBalanceProto.Response]

      res.getResultCase.name match {
        case "SUCCESS" => {
          assert(true)
        }
        case "ERROR" =>
          assert(false)
      }
    }
  }*/
}
