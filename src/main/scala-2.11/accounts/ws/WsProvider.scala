package accounts.ws

import common.utils.Config

object WsProvider {
  val clients: Map[String, WsClient] = Config.topConnectAccounts.map(acc => {
    val wsClient = new WsClient(acc._1.tcUserName, acc._1.tcPassword, acc._1.accountServiceId)
    acc._1.tcUserName -> wsClient
  }).toMap
}
