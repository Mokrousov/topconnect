package common.http.telecom

import spray.json.DefaultJsonProtocol

case class SwitchTelecomProvider(merchantId: String, provider: String)

trait TelecomProtocols extends DefaultJsonProtocol{
  implicit val switchTelecomProvider2RequestFormat = jsonFormat2(SwitchTelecomProvider.apply)
}
