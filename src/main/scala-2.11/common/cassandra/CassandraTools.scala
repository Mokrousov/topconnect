package common.cassandra

import akka.actor.{Actor, ActorLogging}
import akka.pattern.Patterns.pipe
import com.datastax.driver.core.{ResultSetFuture, Row, Session}

import scala.concurrent.Future
import scala.util.{Failure, Success}
import common.utils._

import scala.collection.JavaConversions._
import common.utils.Implicits.cassandraExecutionContext

trait CassandraTools extends Actor with ActorLogging {

  /**
    * Sends reply to sender
    */
  def reply[T](result: Future[T]): Unit = {
    result onComplete {
      case Success(s) =>  log.info("\nReply: \n" + s.getClass.getCanonicalName + "\n" + s.toString);
      case Failure(t) => log.error("\nReply error: \n" + t.getMessage);
    }
    pipe(result, context.dispatcher) to sender()
  }

  /**
    * Wraps Cassandra ResultSetFuture to Scala Future. Returns nothing. Needed for update and insert statements. But not recommend.
    */
  def none(resultSet: ResultSetFuture): Future[Unit] = resultSet.map(_ => Unit)

  /**
    * Wrap Cassandra ResultSetFuture to Scala Future to return one row from result set.
    */
  def one(resultSet: ResultSetFuture): Future[Option[Row]] = resultSet.map(rs => Option(rs.one()))

  /**
    * Wraps result set to Scala Future to return all rows from result set.
    */
  def all(resultSet: ResultSetFuture): Future[List[Row]] = resultSet.map(rs => if(rs == null) List() else rs.all().toList)

  /**
    * Wraps result set to Scala Future to return true or false.
    */
  def oneApply(resultSet: ResultSetFuture): Future[Boolean] = resultSet.map(_.wasApplied())
}
