logLevel := Level.Warn

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.3")
addSbtPlugin("com.typesafe.sbt" % "sbt-aspectj" % "0.10.6")