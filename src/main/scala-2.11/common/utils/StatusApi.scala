package common.utils

import java.time.format.DateTimeFormatter
import java.time.{Duration, ZoneId, ZonedDateTime}
import scala.language.postfixOps

object StatusApi {
  private val utcZoneId = ZoneId.of("UTC")
  val started = ZonedDateTime.now(utcZoneId)
  def uptime = Duration.between(started, ZonedDateTime.now(utcZoneId))
  val gitinfo = {
    import ca.aaronlevin.gitrev._
    gitHash
  }

  def asMap: Map[String, String] = Map(
    "started" -> started.format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
    "uptime" -> uptime.toString,
    "commit hash" -> gitinfo
  )
}