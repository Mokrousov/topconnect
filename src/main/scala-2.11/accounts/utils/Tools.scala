package accounts.utils

import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.DateTimeFormat

import scala.math.BigDecimal.RoundingMode

object Tools {
  def rountingDown(bd: String): String = BigDecimal(bd).setScale(2, RoundingMode.DOWN).toString

  def nextDay(str: String): String = {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
    val date = DateTime.parse(str, formatter)
    date.plusDays(1).toString(formatter)
  }

  /**
    * "-3.14 EUR" -> ("-3.14","EUR")
    */
  def parseAmount(amount: String): (String, String) = {
    val res = amount.split(" +")
    (res(0), res(1))
  }

  /**
    * Format date to UTC
    */
  def toUTC(date: String): String = {
    val fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.forID("Europe/Tallinn"))
    val fmtUTC = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZone(DateTimeZone.UTC)
    fmt.parseDateTime(date).toString(fmtUTC)
  }
}

