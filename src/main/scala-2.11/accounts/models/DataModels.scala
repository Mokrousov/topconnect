package accounts.models

import java.util.Date

import com.solution.telecom.proto.GetHistoryProto.History

case class HistoryHazelcast(hash: String, history: Seq[History], pages: Int, page: Int)
case class AccountModel(accountId: String, merchantId: String, iccid: String, msisdn: String, date: Date)